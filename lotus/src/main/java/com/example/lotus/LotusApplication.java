package com.example.lotus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.lotus.entities.Customer;
import com.example.lotus.repositories.CustomerRepository;

@SpringBootApplication
public class LotusApplication implements CommandLineRunner {

	@Autowired
	private CustomerRepository customerRepository;

	public static void main(String[] args) {
		SpringApplication.run(LotusApplication.class, args);
	}

	public void run(String... args) throws Exception {
		customerRepository.deleteAll();

		// save a couple of customers
		customerRepository.save(new Customer("Alice"));
		customerRepository.save(new Customer("Bob"));
		customerRepository.save(new Customer("Smith"));
		customerRepository.save(new Customer("Charles"));
		customerRepository.save(new Customer("Calli"));
		customerRepository.save(new Customer("Ellwood"));
		customerRepository.save(new Customer("Perren"));
		customerRepository.save(new Customer("Georas"));
		customerRepository.save(new Customer("Hermina"));
		customerRepository.save(new Customer("Annalee"));
		customerRepository.save(new Customer("Collin"));
		customerRepository.save(new Customer("Lorraine"));
		customerRepository.save(new Customer("Linea"));
		customerRepository.save(new Customer("Brit"));
		customerRepository.save(new Customer("Walden"));
		customerRepository.save(new Customer("Filbert"));
		customerRepository.save(new Customer("Stephani"));
		customerRepository.save(new Customer("Roderick"));
		customerRepository.save(new Customer("Ardyth"));
		customerRepository.save(new Customer("Ash"));
		customerRepository.save(new Customer("Orly"));
		customerRepository.save(new Customer("Dene"));
		customerRepository.save(new Customer("Kial"));
		customerRepository.save(new Customer("Nicoli"));
		customerRepository.save(new Customer("Alfons"));
		customerRepository.save(new Customer("Budd"));
		customerRepository.save(new Customer("Bond"));
		customerRepository.save(new Customer("Jodie"));
		customerRepository.save(new Customer("Derry"));
		customerRepository.save(new Customer("Denis"));
	}

}
