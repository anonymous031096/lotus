package com.example.lotus.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.lotus.entities.Customer;
import com.example.lotus.repositories.CustomerRepository;

@Service
public class CustomerService {

	private static final String PERCENT = "%";

	@Autowired
	private CustomerRepository customerRepository;

	public List<Customer> search(String keyword) {
		return customerRepository.findTop3ByNameLike(PERCENT + keyword + PERCENT);
	}
}
