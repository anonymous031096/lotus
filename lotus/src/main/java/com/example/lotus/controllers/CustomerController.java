package com.example.lotus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.lotus.entities.Customer;
import com.example.lotus.services.CustomerService;

@RestController
@RequestMapping("api/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "search/{keyword}", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Customer>> search(@PathVariable("keyword") String keyword) {
		try {
			return new ResponseEntity<List<Customer>>(customerService.search(keyword), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Customer>>(HttpStatus.BAD_REQUEST);
		}
	}
}
