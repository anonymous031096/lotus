package com.example.lotus.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer", catalog = "lotus")
public class Customer implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	public Integer id;
	public String name;

	public Customer() {
	}

	public Customer(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 450)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}