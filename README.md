# For Q1
in folder 'test1'. Im follow this algorithm:
1. Tạo 1 stack tạm tmpStack.
2. Lặp input stack khi NOT empty:
* -- Lấy phần tử trên cùng ra khỏi stack lưu vào biến tạm tmp
* -- Lặp tmpStack khi NOT empty và phần tử trên cùng của input > tmp, lấy phần tử trên cùng của tmpStack và đẩy vào input
* -- Đẩy tmp vào tmpStack

3. Sắp xếp xong tmpStack

* exam input: [34, 3, 23]

* Element taken out: 23
* input: [34, 3]
* tmpStack: [23]

* Element taken out: 3
* input: [34, 23]
* tmpStack: [3]

* Element taken out: 23
* input: [34]
* tmpStack: [3, 23]

* Element taken out: 34
* input: []
* tmpStack: [3, 23, 34]

* final sorted list: [3, 23, 34]


# For Q2
in folder 'lotus'
- import project 'lotus' to eclipse or other IDE
- choose import exist gradle project
- and open application.properties file config folow:
+ spring.datasource.url=jdbc:mysql://localhost:3306/lotus -> url database
+ spring.datasource.username=root -> username db
+ spring.datasource.password= -> pass db
- and now run project and using rest api: localhost:8080/api/customer/search/{keyword}

mô tả hệ thống:
Model 'Customer'*  gồm có id, name dùng mysql để lưu trữ.
* Sử dụng Entity để mapping model giữa spring và database.
* @RestController trỏ ra 1 rest là: localhost:8080/api/customer/search/{keyword}
* Khi client dùng rest vs 'keyword' thì phần service sẽ gọi hàm trong jpaRepository để lấy đc top 3 name mà có ký tự trùng vs 'keyword', cụ thể dùng hàm 'findTop3ByNameLike(String keyword)' trong jpaRepository
* Rest sẽ trả về json bao gồm id, name của Customer
* Ví dụ:
* call localhost:8080/api/customer/search/ni
* returns:
[
    {
        "id": 53,
        "name": "Stephani"
    },
    {
        "id": 60,
        "name": "Nicoli"
    },
    {
        "id": 66,
        "name": "Denis"
    }
]