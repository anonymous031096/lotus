package test1;

import java.util.*;

class SortStack {

	public static Stack<Integer> sort1(Stack<Integer> input) {
		Stack<Integer> tmpStack = new Stack<Integer>();
		while (!input.isEmpty()) {
			int tmp = input.pop();
			while (!tmpStack.isEmpty() && tmpStack.peek() > tmp) {
				input.push(tmpStack.pop());
			}

			tmpStack.push(tmp);
		}
		return tmpStack;
	}

	static void sortedInsert(Stack<Integer> s, int x) {
		if (s.isEmpty() || x > s.peek()) {
			s.push(x);
			return;
		}

		int temp = s.pop();
		sortedInsert(s, x);

		s.push(temp);
	}

	static void sort2(Stack<Integer> s) {
		if (!s.isEmpty()) {
			int x = s.pop();

			sort2(s);

			sortedInsert(s, x);
		}
	}

	static void printStack(Stack<Integer> s) {
		ListIterator<Integer> lt = s.listIterator();

		while (lt.hasNext())
			lt.next();

		while (lt.hasPrevious())
			System.out.print(lt.previous() + " ");
	}

	public static void main(String args[]) {
		Stack<Integer> input = new Stack<Integer>();
		Random r = new Random();
		// sorting 1
		for (int i = 0; i < 500; i++) {
			input.add(r.nextInt(1000));
		}

		System.out.println("Stack elements before sorting 1: ");
		printStack(input);

		Stack<Integer> tmpStack = sort1(input);
		System.out.println(" \n\nStack elements after sorting 1:");
		printStack(tmpStack);

		// sorting 2
		for (int i = 0; i < 500; i++) {
			input.add(r.nextInt(1000));
		}

		System.out.println("\n\nStack elements before sorting 2: ");
		printStack(input);

		sort2(input);
		System.out.println(" \n\nStack elements after sorting 2:");
		printStack(input);
	}
}